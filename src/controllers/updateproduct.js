const productschema = require("../schema/product");

const updateproduct = async ({ id, data }) => {
  if (id == undefined || data == undefined)
    return {
      result:
        "please send ID and all values(id, name, stock, price) that you want to update",
      code: 400,
    };
  try {
    let result = await productschema.findOneAndUpdate({ id }, data, {
      new: true,
    });
    return { result, code: 200 };
  } catch (e) {
    return { result: e, code: 500 };
  }
};
module.exports = updateproduct;
