const productschema = require("../schema/product");

const getallproduct = async () => {
  try {
    let result = await productschema.find({});
    return { result, code: 200 };
  } catch (e) {
    return { result: e, code: 500 };
  }
};

module.exports = getallproduct;
