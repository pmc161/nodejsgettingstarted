const productschema = require("../schema/product");

const deleteproduct = async ({ id }) => {
  if (id == undefined)
    return {
      result: "please send ID",
      code: 400,
    };
  try {
    let result = await productschema.findOneAndRemove({ id });
    return { result, code: 200 };
  } catch (e) {
    return { result: e, code: 500 };
  }
};

module.exports = deleteproduct;
