const productschema = require("../schema/product");
const validid = require("../utils/validid");

const addproduct = async ({ id, name, stock, price }) => {
  if (
    id == undefined ||
    name == undefined ||
    stock == undefined ||
    price == undefined
  )
    return {
      result: "please send all values(id, name, stock, price)",
      code: 400,
    };
  let idvalid = await validid(id);

  if (idvalid == true) {
    try {
      let result = await new productschema({
        id,
        name,
        stock,
        price,
      }).save();

      return { result, code: 201 };
    } catch (e) {
      return { result: e, code: 500 };
    }
  } else {
    return { result: "You can not set one id for two products", code: 400 };
  }
};

module.exports = addproduct;
