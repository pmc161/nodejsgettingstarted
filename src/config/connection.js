const mongoose = require("mongoose");

mongoose
  .connect(
    "mongodb+srv://pmc:pmc123@cluster0.wkaxv.mongodb.net/pmc-practice?retryWrites=true&w=majority",
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then(() => {
    console.log("connected");
  })
  .catch((e) => {
    console.log("fail " + e);
  });
