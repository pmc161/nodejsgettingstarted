const productschema = require("../schema/product");

const validid = async (id) => {
  let valid = true;

  let allid = await productschema.find({}, { id: 1, _id: 0 });

  allid.forEach((product) => {
    if (product.id == id) valid = false;
  });
  return valid;
};

module.exports = validid;
