const mongoose = require("mongoose");

const reqNumber = {
  type: Number,
  require: true,
};

const productschema = mongoose.Schema(
  {
    id: reqNumber,
    name: { type: String, require: true },
    stock: reqNumber,
    price: reqNumber,
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("product", productschema);
