const express = require("express");
require("./config/connection");
const product = require("./routes/product");

const app = express();
app.use(express.json());

const port = process.env.PORT || 3000;

app.use(express.json());
app.use("/product", product);

app.listen(port, () => {
  console.log("Server is running on " + port);
});
