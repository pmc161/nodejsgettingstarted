const express = require("express");
const addproduct = require("../controllers/addproduct");
const deleteproduct = require("../controllers/deleteproduct");
const getallproduct = require("../controllers/getallproduct");
const getspecificproduct = require("../controllers/getspecificproduct");
const updateproduct = require("../controllers/updateproduct");

const router = express.Router();

router.get("/all", async (req, res) => {
  let { code, result } = await getallproduct();
  res.status(code).send(result);
});

router.get("/", async (req, res) => {
  let { code, result } = await getspecificproduct(req.body);
  res.status(code).send(result);
});

router.post("/", async (req, res) => {
  let { code, result } = await addproduct(req.body);
  res.status(code).send(result);
});

router.put("/", async (req, res) => {
  let { code, result } = await updateproduct(req.body);
  res.status(code).send(result);
});

router.delete("/", async (req, res) => {
  let { code, result } = await deleteproduct(req.body);
  res.status(code).send(result);
});

module.exports = router;
